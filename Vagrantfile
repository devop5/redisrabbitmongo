# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.define :redisrabbitmongo do |node|
    bridge = ENV['VAGRANT_BRIDGE']
    bridge ||= 'eth0'

    env  = ENV['PUPPET_ENV']
    env ||= 'dev'
    
    node.vm.box = 'precise64'
    node.vm.box_url = 'https://artifactory.ozforex.local/artifactory/vagrant/ubuntu1404-puppetlatest-1.0.17.box'
    node.vm.box_download_insecure = true
    node.vm.network :public_network, :bridge => bridge
    node.vm.network "private_network", ip: "10.1.1.50",
        auto_config: false
    node.vm.hostname = 'redisrabbitmongo.local'

    node.vm.provider :virtualbox do |vb|
      vb.customize ['modifyvm', :id, '--memory', 2048, '--cpus', 2]
    end

    node.vm.provision :puppet do |puppet|
      puppet.manifests_path = 'manifests'
      puppet.manifest_file  = 'default.pp'
      puppet.options = "--modulepath=/vagrant/modules:/vagrant/static-modules --hiera_config /vagrant/hiera_vagrant.yaml --environment=#{env}"
    end
  end
  config.vm.network "forwarded_port", guest: 27017, host: 27017
  config.vm.network "forwarded_port", guest: 5672, host: 5672
  config.vm.network "forwarded_port", guest: 15672, host: 15672
  config.vm.network "forwarded_port", guest: 16379, host: 16379
  config.vm.network "forwarded_port", guest: 16380, host: 16380
  config.vm.network "forwarded_port", guest: 26379, host: 26379
  if Vagrant.has_plugin?("vagrant-proxyconf")
      config.proxy.http = "http://10.1.1.1:3128/"
      config.proxy.https      = "http://10.1.1.1:3128/"
      config.proxy.no_proxy = "localhost,127.0.0.1,.local"
  end
end
