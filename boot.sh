#!/bin/bash
gem install bundle
bundle install
bundle exec librarian-puppet install
vagrant plugin install vagrant-proxyconf
vagrant up --provider virtualbox
