node 'redisrabbitmongo.local' {
  class { 'redis::install' : }

  redis::server { 'redis_1' :
      redis_port => '16379',
      running => true,
      enabled => true,
  }

  redis::server { 'redis_2' :
      redis_port => '16380',
      running => true,
      enabled => true,
      slaveof => '127.0.0.1 16379',
  }

  redis::sentinel { 'sentinel' :
    sentinel_port => '26379',
    monitors =>
    {
      'mymaster' =>
      {
        master_host             => '127.0.0.1',
        master_port             => 16379,
        quorum                  => 1,
        down_after_milliseconds => 300,
        parallel-syncs          => 1,
        failover_timeout        => 5000,
      },
    },
    force_rewrite => true,
  }

  class {'::mongodb::server':

  }

  mongodb::db { 'PIP':
    user     => 'developer',
    password => 'S3uHaXlDYy!b',
  }

  mongodb::db { 'PAP':
    user     => 'developer',
    password => 'S3uHaXlDYy!b',
  }

  class { '::rabbitmq':
    service_manage    => false,
    port              => '5672',
    delete_guest_user => true,
    admin_enable => true,
  }

  rabbitmq_user { 'admin':
    admin    => true,
    password => 'admin',
    tags     => ['API'],
  }

  rabbitmq_user { 'AccountingAPI':
    admin    => false,
    password => 'Testuser',
    tags     => ['API'],
  }

  rabbitmq_user_permissions { 'AccountingAPI@/':
    configure_permission => '(mx.servicestack|accounting).*',
    read_permission      => '(mx.servicestack|accounting).*',
    write_permission     => '(mx.servicestack|accounting).*',
  }

  rabbitmq_user { 'AnalyticsAPI':
    admin    => false,
    password => 'Testuser',
    tags     => ['API'],
  }

  rabbitmq_user_permissions { 'AnalyticsAPI@/':
    configure_permission => '(mx.servicestack|analytics).*',
    read_permission      => '(mx.servicestack|analytics).*',
    write_permission     => '(mx.servicestack|analytics).*',
  }

  rabbitmq_user { 'AuthenticationAPI':
    admin    => false,
    password => 'Testuser',
    tags     => ['API'],
  }

  rabbitmq_user_permissions { 'AuthenticationAPI@/':
    configure_permission => '(mx.servicestack|authentication).*',
    read_permission      => '(mx.servicestack|authentication).*',
    write_permission     => '(mx.servicestack|authentication).*',
  }

  rabbitmq_user { 'AuthorisationAPI':
    admin    => false,
    password => 'Testuser',
    tags     => ['API'],
  }

  rabbitmq_user_permissions { 'AuthorisationAPI@/':
    configure_permission => '(mx.servicestack|authorisation).*',
    read_permission      => '(mx.servicestack|authorisation).*',
    write_permission     => '(mx.servicestack|authorisation).*',
  }

  rabbitmq_user { 'BeneficiaryAPI':
    admin    => false,
    password => 'Testuser',
    tags     => ['API'],
  }

  rabbitmq_user_permissions { 'BeneficiaryAPI@/':
    configure_permission => '(mx.servicestack|beneficiary).*',
    read_permission      => '(mx.servicestack|beneficiary).*',
    write_permission     => '(mx.servicestack|beneficiary).*',
  }

  rabbitmq_user { 'DealsAPI':
    admin    => false,
    password => 'Testuser',
    tags     => ['API'],
  }

  rabbitmq_user_permissions { 'DealsAPI@/':
    configure_permission => '(mx.servicestack|deals).*',
    read_permission      => '(mx.servicestack|deals).*',
    write_permission     => '(mx.servicestack|deals).*',
  }

  rabbitmq_user { 'QuoteAPI':
    admin    => false,
    password => 'Testuser',
    tags     => ['API'],
  }

  rabbitmq_user_permissions { 'QuoteAPI@/':
    configure_permission => '(mx.servicestack|quote).*',
    read_permission      => '(mx.servicestack|quote).*',
    write_permission     => '(mx.servicestack|quote).*',
  }

  rabbitmq_user { 'RefDataAPI':
    admin    => false,
    password => 'Testuser',
    tags     => ['API'],
  }

  rabbitmq_user_permissions { 'RefDataAPI@/':
    configure_permission => '(mx.servicestack|refdata).*',
    read_permission      => '(mx.servicestack|refdata).*',
    write_permission     => '(mx.servicestack|refdata).*',
  }

  rabbitmq_user { 'UserAPI':
    admin    => false,
    password => 'Testuser',
    tags     => ['API'],
  }

  rabbitmq_user_permissions { 'UserAPI@/':
    configure_permission => '(mx.servicestack|user).*',
    read_permission      => '(mx.servicestack|user).*',
    write_permission     => '(mx.servicestack|user).*',
  }

}
