# Intro
This project manages a sandbox for [redisrabbitmongo](url)

# Pre-requisites
Vagrant, Rubygems, virtualbox, ruby2.devkit

Download and install by browsing to this URL in Internet Explorer (do not use Chrome or Firefox for this!)
http://boxstarter.org/package/vagrant,rubygems,virtualbox,ruby2.devkit

# Installation steps
```bash
  # install require gems, puppet modules and fire up vagrant
  $ ./boot.sh or ./boot.cmd in Windows
```

# Usage
This will create a headless Ubuntu Linux VM on your machine using Vagrant and Virtualbox. The VM will have redis, rabbitmq and mongodb installed.
There are also default port forwarding rules, so that the following ports on your local machine are forwarded to the VM automatically:

* 5672 - RabbitMQ
* 15672 - RabbitMQ admin console
* 26379 - Redis Sentinel
* 16379 - Redis (master - instance 1)
* 16380 - Redis (slave - instance 2)
* 27017 - MongoDB

# What this script actually does?
* MongoDB - creates PIP and PAP databases with an admin user (developer/S3uHaXlDYy!b)
* RabbitMQ - creates users for each of the BSL APIs. Also creates a generic admin user (admin/admin) for troubleshooting
* Redis - Installs two instances of Redis (on port 16379 and 16380). Also installs redis sentinel in order for High Availability to work
